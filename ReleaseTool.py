from git import Repo
import os
import time


f = open("ReleaseNoteExample.md")
str = f.read()
configs = str.split(";")
targetBranchName = configs[1]
mergeBranchName = configs[2]
tagName = configs[0]
repoPath = configs[3]

repo = Repo(repoPath)
targetBranch = repo.branches[targetBranchName]
mergeBranch = repo.branches[mergeBranchName]
repo.git.checkout(targetBranchName)
base = repo.merge_base(targetBranch,mergeBranch)
repo.index.merge_tree(targetBranch, base=base)
repo.index.commit(configs[4],parent_commits=(targetBranch.commit,mergeBranch.commit))
repo.create_tag(tagName)

tags = repo.tags
target = tags[len(tags)-1]
versionName = ""
commitMessage = "" 

if target is not None:
    commitMessage = target.commit.message
    versionName = target.name
    fileName = targetBranchName+versionName+'.md'
    time.asctime(time.gmtime(target.commit.committed_date))
    strTime = time.strftime("%Y-%m-%d", time.gmtime(target.commit.committed_date))
   
    p = open(fileName, 'x')
    p.write('# Version '+versionName + '\n')
    p.write('- Last update: '+strTime+'\n')
    p.write(commitMessage)
    p.close()


